﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelTerrain : MonoBehaviour
{

    public GameObject blockPrefab;
    public GameObject blockParent;
    float noiseScale = 16;
    int dimension = 32;
    int height = 16;
    List<UnityEngine.Object> voxels = new List<UnityEngine.Object>();
    // Start is called before the first frame update
    void Start()
    {
        for(int x = 0; x < dimension; x++)
        {
            for (int y = 0; y < dimension; y++)
            {
                for (int z = 0; z < dimension; z++)
                {
                    float curNoise = Mathf.PerlinNoise(x / noiseScale, z / noiseScale) * height;
                    if ( curNoise >= y || curNoise  <= y-14)
                    {
                        GameObject block = Instantiate(blockPrefab, new Vector3(x, y, z), Quaternion.identity);
                        voxels.Add(block);
                        block.transform.SetParent(blockParent.transform);
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static float Perlin3D(float x, float y, float z)
    {
        float ab = Mathf.PerlinNoise(x, y);
        float bc = Mathf.PerlinNoise(y, z);
        float ac = Mathf.PerlinNoise(x, z);

        float ba = Mathf.PerlinNoise(y, x);
        float cb = Mathf.PerlinNoise(z, y);
        float ca = Mathf.PerlinNoise(z, x);

        return (ab + bc + ac + ba + cb + ca) / 6f;
    }
}
