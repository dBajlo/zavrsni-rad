﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Noise 
{
	static List<double> perlinValues;
	static int myHeight;
	static bool calculateStatistics = false;
	

	public static float[,,] GenerateNoiseMap(int width, int height, int length, bool isSphere, bool usesFalloff, float thresholdDensity,
		float falloffPower, int octaves, float scale, float persistence, float lacunarity, int perlinNum, Vector3 offset,
		List<Structs.TerrainChange> terrainChanges, bool usesEditAlgorithm2)
	{
		myHeight = height;
		//Used for calculating average and dispersion
		perlinValues = new List<double>();

		float[,,] noiseMap = new float[width + 1, height + 1, length + 1];
		float minDensity = float.MaxValue;
		float maxDensity = float.MinValue;

		Vector3 center = new Vector3(width / 2, height / 2, length / 2);
		float radius = width / 2;
		float squareRadius = radius * radius;

		float pointDensity;
		for (int x = 0; x < width + 1; x++)
		{
			for (int y = 0; y < height + 1; y++)
			{
				for (int z = 0; z < length + 1; z++)
				{
					if (IsBorder(x, y, z, 1, width, height, length) || isSphere && SquareDistance(center, new Vector3(x, y, z)) > squareRadius)
					{
						pointDensity = 1;
					}
					else
					{
						Structs.TerrainChange change;
						if (!usesEditAlgorithm2)
						{
							change = hasChanged(x, y, z, terrainChanges);
						}
						else
						{
							//if we're using the 2nd algorithm then we skip changes 
							change = new Structs.TerrainChange(Vector3.zero, -1f, false);
						}
						if (change.getRadius() > 0f)
						{
							if (change.getMaterial())
							{
								pointDensity = 0;
							}
							else
							{
								pointDensity = 1;
							}
						}
						//float bounds = thresholdDensity * falloffPower;
						//pointDensity *= thresholdDensity + (1 / bounds - bounds) * (float)y / (height);
						else
						{
							pointDensity = GetPointDensity(new Vector3(x, y, z), octaves, scale, 
								persistence, lacunarity, offset, perlinNum, usesFalloff);
							if (usesFalloff)
							{
								pointDensity *= falloffPower * ((float)y / (height));
							}
						}
					}



					if (calculateStatistics && minDensity > pointDensity)
					{
						minDensity = pointDensity;
					}
					if (calculateStatistics &&  maxDensity < pointDensity)
					{
						maxDensity = pointDensity;
					}
					noiseMap[x, y, z] = pointDensity;
				}
			}
		}
		if (usesEditAlgorithm2)
		{
			Alg2(terrainChanges, width, height, length, noiseMap, thresholdDensity);
		}

		if (calculateStatistics)
		{
			double perlinAvg = Statistics.AverageValue(perlinValues);
			double perlinStd = Statistics.StandardDeviation(perlinValues);
			MonoBehaviour.print(perlinAvg + " +/- " + perlinStd);
			MonoBehaviour.print("max: " + maxDensity + " min: " + minDensity);
		}
		
		return noiseMap;
    }

	public static void Alg2(List<Structs.TerrainChange> terrainChanges, int width, int height, int length, float[,,] noiseMap, float thresholdDensity)
	{
		
		foreach(Structs.TerrainChange change in terrainChanges)
		{
			EditValues(change, width, height, length, noiseMap);
			
		}
		
	}

	public static void EditValues(Structs.TerrainChange change, int width, int height,
		int length, float[,,] noiseMap)
	{
		Vector3 position = change.getPosition();
		float radius = change.getRadius();
		float sqrRadius = radius * radius;
		Vector3Int upperLimit = new Vector3Int((int)Mathf.Min(width, position.x + radius),
											   (int)Mathf.Min(height, position.y + radius),
											   (int)Mathf.Min(length, position.z + radius));
		Vector3Int lowerLimit = new Vector3Int((int)Mathf.Max(0, position.x - radius),
											   (int)Mathf.Max(0, position.y - radius),
											   (int)Mathf.Max(0, position.z - radius));
		for (int i = lowerLimit.x; i <= upperLimit.x; i++)
		{
			for (int j = lowerLimit.y; j <= upperLimit.y; j++)
			{
				for (int k = lowerLimit.z; k <= upperLimit.z; k++)
				{
					float sqrDistance = SquareDistance(new Vector3(i, j, k), position);
					if (sqrDistance < sqrRadius)
					{
						if (change.getMaterial())
						{
							noiseMap[i, j, k] = 0;
						}
						else
						{
							noiseMap[i, j, k] = 1;
						}
					}

				}
			}
		}
	}

	public static bool IsBorder(int x, int y, int z, int borderWidth, int width, int height, int length)
	{
		for (int i = 0; i < borderWidth; i++)
		{
			if (x == 0 + i || y == 0 + i || z == 0 + i || x == width - i || y == height - i || z == length - i)
			{
				return true;
			}
		}

		return false;
	}

	public static Structs.TerrainChange hasChanged(int x, int y, int z, List<Structs.TerrainChange> terrainChanges)
	{
		int numChanges = terrainChanges.Count;
		for(int i = numChanges - 1; i >= 0; i--)
		{
			//iterate backwards to get most recent changes
			Structs.TerrainChange change = terrainChanges[i];
			Vector3 position = change.getPosition();
			float radius = change.getRadius();
			//check if inside cube side 2r
			if(x < (position.x + radius) && x > (position.x - radius)
				&& y < (position.y + radius) && y > (position.y - radius)
				&& z < (position.z + radius) && z > (position.z - radius))
			{
				//check if inside sphere r
				if(SquareDistance(new Vector3(x,y,z), position) < radius*radius)
				{
					return change;
				}
			}
		}
		//if the radius is negative the change is invalid
		return new Structs.TerrainChange(Vector3.zero, -1f, false);
	}

	public static float SquareDistance(Vector3 a, Vector3 b)
	{
		Vector3 vector = new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
		return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
	}


	

	public static float GetPointDensity(Vector3 coords, int octaves, float scale, float persistence, 
		float lacunarity, Vector3 offset, int perlinNum, bool usesFalloff)
	{
		float density = 0;
		float amplitude = 1;
		float frequency = 1;
		float noise = 0;
		for (int i = 0; i < octaves; i++)
		{
			noise = Perlin3D(((float)coords.x + offset.x) / scale * frequency, 
							((float)coords.y + offset.y) / scale * frequency, 
							((float)coords.z + offset.z) / scale * frequency, perlinNum) * amplitude;

			if (i == 0)
			{
				density += noise;
			}
			//reduce the influence of octaves in greater heights to prevent floating elements
			else if (!usesFalloff || coords.y < myHeight / 2)
			{	
				//scale noise to range [-0.5A, 0.5A]
				density += noise - amplitude * 0.5f;
			}
			amplitude *= persistence;
			frequency *= lacunarity;
		}
		return density;
	}


	public static float Perlin3D(float x, float y, float z, int perlinNum)
	{
		float ab = Mathf.PerlinNoise(x, y);
		float bc = Mathf.PerlinNoise(y, z);
		
		float sum = ab + bc;
		if (perlinNum > 2)
		{
			float ac = Mathf.PerlinNoise(x, z);
			sum += ac;
		}
		if (perlinNum > 3)
		{
			float ba = Mathf.PerlinNoise(y, x);
			sum += ba;
		}
		if (perlinNum > 4)
		{
			float cb = Mathf.PerlinNoise(z, y);
			sum += cb;
		}
		if (perlinNum > 5)
		{
			float ca = Mathf.PerlinNoise(z, x);
			sum += ca;
		}

		float result = sum / (float)perlinNum;
		perlinValues.Add(result);
		return result;
	}


}
