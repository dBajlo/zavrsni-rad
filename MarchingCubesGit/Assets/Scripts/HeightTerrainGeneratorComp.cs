﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightTerrainGeneratorComp : MonoBehaviour
{
    public ComputeShader populateNoiseMap2D;
    float[] noiseMap;


    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();
    MeshFilter meshFilter;
    MeshCollider meshCollider;

    public float lacunarity = 2;
    public float persistence = 0.5f;
    public float frequency = 1;
    public float amplitude = 1;
    public int octaves = 3;
    public int offsetX = 15;

    int numThreadGroups = 32;
    int numOfThreads = 32;  //if changed, change in PopulateNoiseMap2D.compute as well

    bool isLinear = true;
    bool runTest = true;
   
    void Start()
    {
        meshCollider = GetComponent<MeshCollider>();
        meshFilter = GetComponent<MeshFilter>();
        if (runTest)
        {
            RunTest();
        }
        else
        {
            GenerateTerrain();
        }
        
    }

    void GenerateTerrain()
    {
        ClearMeshData();
        
        if (isLinear)
        {
            
            GenerateNoiseSelf(true);
            isLinear = false;
            
        }
        else
        {
            
            GetNoiseMap(true);
            isLinear = true;
            
        }
        
        
        
        //GenerateMeshData();
        //BuildMesh();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            
            GenerateTerrain();
        }
    }

    static void PrintArray(float[] array)
    {
        for(int i = 0; i < array.Length; i++)
        {
            print(array[i]);
        }
    }

    void GetNoiseMap(bool debug)
    {
        int numEle = numOfThreads * numOfThreads * 1 * numThreadGroups * numThreadGroups;
        float startTime = Time.realtimeSinceStartup;
        
        
        ComputeBuffer buffer = new ComputeBuffer(numEle, sizeof(float));

        

        //buffer.SetData(noiseMap);
        
        populateNoiseMap2D.SetBuffer(0, "points", buffer);
        populateNoiseMap2D.SetInt("numThreadGroups", numThreadGroups);
        populateNoiseMap2D.SetInt("octaves", octaves);
        populateNoiseMap2D.SetFloat("lacunarity", lacunarity);
        populateNoiseMap2D.SetFloat("persistence", persistence);
        populateNoiseMap2D.SetFloat("frequency", frequency);
        populateNoiseMap2D.SetFloat("amplitude", amplitude);
        populateNoiseMap2D.SetInt("offsetX", offsetX);
        


        populateNoiseMap2D.Dispatch(0, numThreadGroups, numThreadGroups, 1);
        noiseMap = new float[numEle];




        buffer.GetData(noiseMap);
        buffer.Release();

        if (debug)
        {
            float endTime = Time.realtimeSinceStartup;
            print("Compute: " + (endTime - startTime));

            //PrintArray(noiseMap);
            print(numEle);
        }

        
    }

    void GenerateNoiseSelf(bool debug)
    {

        float startTime = Time.realtimeSinceStartup;
        int numEle = numOfThreads * numOfThreads * 1 * numThreadGroups * numThreadGroups;
        noiseMap = new float[numEle];
        int width = numOfThreads * numThreadGroups;
        int length = width;

        float tempAmplitude;
        float tempFrequency;
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < length; z++)
            {
                tempAmplitude = amplitude;
                tempFrequency = frequency;
                //float3 position = {50.5, 300.1, 33.1};
                float value = 0;
                for (int i = 0; i < octaves; i++)
                {
                    
                    value += Mathf.PerlinNoise(x * tempFrequency + 0.1f, z * tempFrequency + 0.1f)*tempAmplitude;
                    tempAmplitude *= persistence;
                    tempFrequency *= lacunarity;

                }


                noiseMap[x * length + z] = value;
            }
        }
        if (debug)
        {
            float endTime = Time.realtimeSinceStartup;
            print("Duration of linear: " + (endTime - startTime));
        }
        

    }

    void RunTest()
    {
        List<double> linearTimesList = new List<double>();
        List<double> computeTimesList = new List<double>();
        int numIter = 10;
        double timeStart0 = Time.realtimeSinceStartup;
        print("Num vertices: " + numOfThreads * numOfThreads * 1 * numThreadGroups * numThreadGroups);
        for (int i = 0; i < numIter; i++)
        {
            double timeStart = Time.realtimeSinceStartup;
            GetNoiseMap(false);
            computeTimesList.Add(Time.realtimeSinceStartup - timeStart);
        }
        print("Compute: " + Statistics.GetAverageAndDeviationString(computeTimesList));
        for (int i = 0; i < numIter; i++)
        {
            double timeStart = Time.realtimeSinceStartup;
            GenerateNoiseSelf(false);
            linearTimesList.Add(Time.realtimeSinceStartup - timeStart);
        }
        print("Linear: " + Statistics.GetAverageAndDeviationString(linearTimesList));
        print("Ubrzanje: " + System.Math.Round(Statistics.AverageValue(linearTimesList) / Statistics.AverageValue(computeTimesList),2));
        print("Trajanje: " + System.Math.Round(Time.realtimeSinceStartup - timeStart0, 2));
    }


    void ClearMeshData()
    {
        vertices.Clear();
        triangles.Clear();
        

    }

    void GenerateMeshData()
    {
        int numel = noiseMap.Length;
        int width = numOfThreads * numThreadGroups;
        float scale = 1.5f;

        for (int i = 0; i < numel; i++)
        {
            int x = i/ width;
            int z = i % width;
            vertices.Add(new Vector3(x, noiseMap[i]*scale, z));
            if(x < width - 1 && z < width - 1){
                triangles.Add(i);
                triangles.Add(i+width+1);
                triangles.Add(i+width);
                triangles.Add(i+width+1);
                triangles.Add(i);
                triangles.Add(i+1);
            }
        }
    }

    void BuildMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        //to make it look nice and neat
        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
        print(mesh.vertices.Length);
    }
}
