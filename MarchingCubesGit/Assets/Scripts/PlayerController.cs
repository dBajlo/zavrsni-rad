﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 7;
    public float mouseSensitivity = 100;
    public float jumpForce = 1f;
    float mouseX;
    float mouseY;
    float horizontal;
    float vertical;
    Rigidbody rigidBody;
    Quaternion deltaRotation;
    Vector3 deltaPosition;
    Transform cameraTransform;
    public TerrainGenerator terrainGenerator;
 
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
        cameraTransform.Rotate(-Vector3.right * mouseY * mouseSensitivity * Time.deltaTime);
        if (Input.GetButtonDown("Jump"))
        {
            rigidBody.velocity = Vector3.up * jumpForce;
            rigidBody.useGravity = true;
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            rigidBody.useGravity = !rigidBody.useGravity;
            rigidBody.velocity = Vector3.zero;
        }
        if (Input.GetAxis("Mouse ScrollWheel") != 0f) // forward
        {
            terrainGenerator.editRadius = Mathf.Clamp(terrainGenerator.editRadius + (int)Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")), 1, 15);
            terrainGenerator.editSphere.transform.localScale = Vector3.one * terrainGenerator.editRadius * 2;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            terrainGenerator.smoothTerrain = !terrainGenerator.smoothTerrain;
            terrainGenerator.RedrawTerrain();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            terrainGenerator.flatShaded = !terrainGenerator.flatShaded;
            terrainGenerator.RedrawTerrain();
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            terrainGenerator.octaves = Mathf.Max(1,(terrainGenerator.octaves + 1) % TerrainGenerator.maxOctaves);
            terrainGenerator.GenerateTerrain();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            terrainGenerator.usesFalloff = !terrainGenerator.usesFalloff;
            terrainGenerator.GenerateTerrain();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            terrainGenerator.useVoxel = !terrainGenerator.useVoxel;
            terrainGenerator.RedrawTerrain();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            terrainGenerator.offsetX += 5;
            terrainGenerator.GenerateTerrain();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            terrainGenerator.offsetX -= 5;
            terrainGenerator.GenerateTerrain();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            terrainGenerator.changes.Clear();
            terrainGenerator.GenerateTerrain();
        }

    }

    private void FixedUpdate()
    {
        deltaRotation = Quaternion.Euler(Vector3.up*mouseX*Time.fixedDeltaTime*mouseSensitivity);
        rigidBody.MoveRotation(rigidBody.rotation*deltaRotation);

        deltaPosition = ((transform.forward * vertical) + (transform.right * horizontal)) * Time.fixedDeltaTime * speed;
        rigidBody.MovePosition(rigidBody.position + deltaPosition);
    }

    void GetInputs()
    {
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }
}
