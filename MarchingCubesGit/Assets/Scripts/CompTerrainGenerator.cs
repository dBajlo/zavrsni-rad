﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompTerrainGenerator : MonoBehaviour
{
    public ComputeShader populateNoiseMap;
    public ComputeShader MarchCubes;
    float[] noiseMap;
    float[,,] selfNoiseMap;


    ComputeBuffer pointBuffer;
    ComputeBuffer triangleBuffer;
    ComputeBuffer counterBuffer;

    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();
    MeshFilter meshFilter;
    MeshCollider meshCollider;

    public float lacunarity = 2;
    public float persistence = 0.5f;
    public int octaves = 3;
    public float offsetX = 0;
    public float offsetY = 0;
    public float offsetZ = 0;
    public float scale = 16;
    public float thresholdDensity = 0.5f;

    int numThreadGroups = 3;
    int numOfThreads = 8;  //if changed, change in PopulateNoiseMap.compute and MarchCubes.compute as well

    public bool isLinear = false;

    void Start()
    {
        meshCollider = GetComponent<MeshCollider>();
        meshFilter = GetComponent<MeshFilter>();
        GenerateTerrain();
    }

    void GenerateTerrain()
    {
        ClearMeshData();
        
        if (isLinear)
        {
            float startTime = Time.realtimeSinceStartup;
            GenerateNoiseSelf();
            GenerateMeshData();
            float endTime = Time.realtimeSinceStartup;
            print("LINEAR " + (endTime - startTime));

        }
        else
        {
            float startTime = Time.realtimeSinceStartup;
            GetNoiseMap();
            float endTime = Time.realtimeSinceStartup;
            print("COMPUTE " + (endTime - startTime));

        }
        //isLinear = !isLinear;
        
        BuildMesh();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {

            GenerateTerrain();
        }
    }

    static void PrintArray(float[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            print(array[i]);
        }
    }

    void GetNoiseMap()
    {
        float startTimeG = Time.realtimeSinceStartup;

        int numEle = numOfThreads * numOfThreads * numOfThreads * numThreadGroups * numThreadGroups * numThreadGroups;
        print("Broj elemenata: " + numEle);

        noiseMap = new float[numEle];

        pointBuffer = new ComputeBuffer(numEle, sizeof(float));


        populateNoiseMap.SetBuffer(0, "points", pointBuffer);
        populateNoiseMap.SetInt("numThreadGroups", numThreadGroups);
        populateNoiseMap.SetInt("octaves", octaves);
        populateNoiseMap.SetFloat("lacunarity", lacunarity);
        populateNoiseMap.SetFloat("persistence", persistence);
        populateNoiseMap.SetFloat("scale", scale);
        populateNoiseMap.SetFloat("offsetX", offsetX);
        populateNoiseMap.SetFloat("offsetY", offsetY);
        populateNoiseMap.SetFloat("offsetZ", offsetZ);
        

        populateNoiseMap.Dispatch(0, numThreadGroups, numThreadGroups, numThreadGroups);



        pointBuffer.GetData(noiseMap);
        float endTimeG = Time.realtimeSinceStartup;
        print("COMPUTE GENERATE: " + (endTimeG - startTimeG));

        //startTimeG = Time.realtimeSinceStartup;




        int width = numOfThreads * numThreadGroups - 1;
        int maxNumTriangles = 5 * width * width * width;
        triangleBuffer = new ComputeBuffer(maxNumTriangles, 
            sizeof(float) * 3 * 3, ComputeBufferType.Append);

        triangleBuffer.SetCounterValue(0);
        MarchCubes.SetFloat("thresholdDensity", thresholdDensity);
        MarchCubes.SetInt("numThreadGroups", numThreadGroups);
        MarchCubes.SetBuffer(0, "points", pointBuffer);
        MarchCubes.SetBuffer(0, "triangles", triangleBuffer);

        

        MarchCubes.Dispatch(0, numThreadGroups, numThreadGroups, numThreadGroups);


        
        counterBuffer = new ComputeBuffer(1, sizeof(int), ComputeBufferType.Raw);
        ComputeBuffer.CopyCount(triangleBuffer, counterBuffer, 0);

        float startTimeB = Time.realtimeSinceStartup;
        int[] triCountArray = { 0 };
        counterBuffer.GetData(triCountArray);
        int numTris = triCountArray[0];

        float endTimeB = Time.realtimeSinceStartup;
        print("Citanej buffera: " + (endTimeB - startTimeB));

        float startTimeT = Time.realtimeSinceStartup;
        Structs.Triangle[] triangleArray = new Structs.Triangle[numTris];
        triangleBuffer.GetData(triangleArray, 0, 0, numTris);


        //float startTimeT = Time.realtimeSinceStartup;
        int i = 0;
        foreach (Structs.Triangle t in triangleArray)
        {
            vertices.Add(t.v0);
            triangles.Add(i++);
            vertices.Add(t.v1);
            triangles.Add(i++);
            vertices.Add(t.v2);
            triangles.Add(i++);
        }

        float endTimeT = Time.realtimeSinceStartup;
        print("TRIANGLES: " + (endTimeT - startTimeT));

        triangleBuffer.Release();
        counterBuffer.Release();
        pointBuffer.Release();


        //print("Broj trokuta " + numTris);
        //endTimeG = Time.realtimeSinceStartup;
        //print("COMPUTE MARCH: " + (endTimeG - startTimeG));
    }

    void GenerateNoiseSelf()
    {


        int k = numOfThreads * numThreadGroups;

        int numEle = numOfThreads * numOfThreads * numOfThreads * numThreadGroups * numThreadGroups * numThreadGroups;
        print("Broj elemenata L: " + numEle);
        selfNoiseMap = new float[k,k,k];

        
        for(int x = 0; x < k; x++)
        {
            for (int y = 0; y < k; y++)
            {
                for (int z = 0; z < k; z++)
                {
                    
                    selfNoiseMap[x,y,z] = GetNoiseForPoint(x,y,z,k);
                }
            }
        }

        float endTimeG = Time.realtimeSinceStartup;



    }

    int GetCubeConfig(float[] cube)
    {
        int configIndex = 0;
        for (int i = 0; i < 8; i++)
        {
            if (cube[i] > thresholdDensity)
            {
                configIndex |= 1 << i;
            }
        }
        return configIndex;
    }

    void MarchCube(Vector3Int position, float[] cube)
    {

        int configIndex = GetCubeConfig(cube);


        if (configIndex == 0 || configIndex == 255)
        {
            return;
        }



        int edgeIndex = 0;
        for (int i = 0; i < 5; i++)
        {
            for (int p = 0; p < 3; p++)
            {
                int index = LookupTable.TriangleTable[configIndex, edgeIndex];
                if (index == -1)
                {
                    return;
                }

                Vector3 vert1 = position + LookupTable.EdgeTable[index, 0];

                Vector3 vert2 = position + LookupTable.EdgeTable[index, 1];
                Vector3 vertPosition;

                vertPosition = (vert1 + vert2) / 2;
                vertices.Add(vertPosition);
                triangles.Add(vertices.Count - 1);
                
                edgeIndex++;
            }
        }

    
    }


    void ClearMeshData()
    {
        vertices.Clear();
        triangles.Clear();
    }

    float GetNoiseForPoint(int x, int y , int z, int k)
    {
        if(x==0 || x==k-1 || y == 0 || y == k - 1 || z == 0 || z == k - 1)
        {
            return 1;
        }
        float amplitude = 1;
        float frequency = 1;
        float value = 0;
        for (int i = 0; i < octaves; i++)
        {

            float tempNoise = Perlin3D((x +offsetX) * frequency / scale, (y + offsetY) * frequency / scale, (z + offsetZ) * frequency / scale);
            if (i == 0)
            {
                value += tempNoise * amplitude;
            }
            else
            {
                value += (tempNoise - 0.5f) * amplitude;
            }

            amplitude *= persistence;
            frequency *= lacunarity;

        }

        return value;
    }

    float Perlin3D(float x, float y, float z)
    {
        return (Mathf.PerlinNoise(x, y) + Mathf.PerlinNoise(x, z)) / 2;
    }
 

    void GenerateMeshData()
    {
        int k = numOfThreads * numThreadGroups;
        for (int x = 0; x < k-1; x++)
        {
            for (int y = 0; y < k-1; y++)
            {
                for (int z = 0; z < k-1; z++)
                {
                    float[] cube = new float[8];
                    for (int i = 0; i < 8; i++)
                    {
                        Vector3Int corner = new Vector3Int(x, y, z) + LookupTable.CornerTable[i];
                        cube[i] = selfNoiseMap[corner.x, corner.y, corner.z];
                    }
                    MarchCube(new Vector3Int(x, y, z), cube);
                }
            }
        }


    }

    void BuildMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        //to make it look nice and neat
        mesh.RecalculateNormals();
        meshFilter.mesh = mesh;
        meshCollider.sharedMesh = mesh;
        print("Broj vrhova "+mesh.vertices.Length);
    }
}
