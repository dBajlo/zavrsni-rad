﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGenerator))]
public class NewBehaviourScript : Editor
{
    public override void OnInspectorGUI()
    {
        TerrainGenerator terrainGenerator = (TerrainGenerator)target;

        //if any value in the editor is changed
        if (DrawDefaultInspector() && terrainGenerator.autoUpdate || GUILayout.Button("Generate"))
        {
            terrainGenerator.GenerateTerrain();
        }

        
    }
}
